build: secret binkey build_mtproxy build_http build_sslmux

clean:
	rm binkey
	rm secret

binkey:
	curl -s https://core.telegram.org/getProxySecret -o binkey

secret:
	head -c 16 /dev/urandom | xxd -ps > secret

build_mtproxy:
	sudo docker build -t mtproxy .

build_http:
	cp hostname http_dummy/hostname
	cd http_dummy && sudo docker build -t http_dummy .

build_sslmux:
	cd sslmux && sudo docker build -t sslmux .

start:
	sudo docker run -d --name=mtproxy --network host --restart=always mtproxy
	sudo docker run -d --name=http_dummy --network host -v `pwd`/http_dummy/data:/data --restart=always http_dummy
	sudo docker run -d --name=sslmux --network host --restart=always sslmux

stop:
	sudo docker rm -f mtproxy; true
	sudo docker rm -f http_dummy; true
	sudo docker rm -f sslmux; true

restart: stop start
	echo "restarted"

logs:
	sudo docker logs mtproxy
	sudo docker logs sslmux
	sudo docker logs http_dummy
