package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/acme/autocert"
)

const (
	htmlIndex = `<!DOCTYPE html>
	<html>
	<head>
		<title>Welcome</title>
	</head>
	<body>
	<h1>Test html page</h1><br><br>
	Sorry, the site is under construction.
	</body>
	</html>`

	httpPort  = ":80"
	httpsPort = "127.0.0.1:8443"
)

var (
	serverHost string
)

func handleIndex(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, htmlIndex)
}

func makeServerFromMux(mux *http.ServeMux) *http.Server {
	// set timeouts so that a slow or malicious client doesn't
	// hold resources forever
	return &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      mux,
	}
}

func makeHTTPServer() *http.Server {
	mux := &http.ServeMux{}
	mux.HandleFunc("/", handleIndex)
	return makeServerFromMux(mux)

}

func makeHTTPToHTTPSRedirectServer() *http.Server {
	handleRedirect := func(w http.ResponseWriter, r *http.Request) {
		newURI := "https://" + serverHost + r.URL.String()
		http.Redirect(w, r, newURI, http.StatusFound)
	}
	mux := &http.ServeMux{}
	mux.HandleFunc("/", handleRedirect)
	return makeServerFromMux(mux)
}

func main() {
	{
		rawHost, err := ioutil.ReadFile("/hostname")
		if err != nil {
			panic("Cannot read hostname: " + err.Error())
		}
		serverHost = strings.TrimSpace(string(rawHost))
		if serverHost == "" {
			fmt.Println("hostname shouldn't be empty")
			os.Exit(1)
		}
	}

	var m *autocert.Manager

	var httpsSrv *http.Server

	hostPolicy := func(ctx context.Context, host string) error {
		if host == serverHost {
			return nil
		}
		return fmt.Errorf("only %s host is allowed", serverHost)
	}

	dataDir := "/data"
	m = &autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		HostPolicy: hostPolicy,
		Cache:      autocert.DirCache(dataDir),
	}

	httpsSrv = makeHTTPServer()
	httpsSrv.Addr = httpsPort
	httpsSrv.TLSConfig = &tls.Config{GetCertificate: m.GetCertificate}

	go func() {
		fmt.Printf("Starting HTTPS server on %s\n", httpsSrv.Addr)
		err := httpsSrv.ListenAndServeTLS("", "")
		if err != nil {
			log.Fatalf("httpsSrv.ListendAndServeTLS() failed with %s", err)
		}
	}()

	var httpSrv *http.Server

	httpSrv = makeHTTPToHTTPSRedirectServer()
	// allow autocert handle Let's Encrypt callbacks over http
	if m != nil {
		httpSrv.Handler = m.HTTPHandler(httpSrv.Handler)
	}

	httpSrv.Addr = httpPort
	fmt.Printf("Starting HTTP server on %s\n", httpPort)
	err := httpSrv.ListenAndServe()
	if err != nil {
		log.Fatalf("httpSrv.ListenAndServe() failed with %s", err)
	}
}
