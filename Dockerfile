FROM ubuntu:16.04

RUN apt-get update && apt-get install -y git curl build-essential libssl-dev zlib1g-dev
RUN git clone https://github.com/TelegramMessenger/MTProxy
WORKDIR /MTProxy
RUN make
WORKDIR /MTProxy/objs/bin/
COPY bootstrap.sh bootstrap.sh
COPY secret secret
COPY binkey binkey

CMD ["sh","bootstrap.sh"]
